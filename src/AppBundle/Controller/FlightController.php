<?php
/**
 * Created by PhpStorm.
 * User: kamill
 * Date: 11/8/17
 * Time: 12:03 PM
 */

namespace AppBundle\Controller;

use Doctrine\ORM\Query\AST\Functions\DateAddFunction;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Flight;
use DateTime;

class FlightController extends FOSRestController
{
    public $flight = array("id" => "1",
        "date" => "08/11/2017",
        "pilotId" => "1",
        "flightType" => "Type of flight",
        "startType" => "Type of start",
        "departure" => [
            "departurePlace" => "HEL",
            "startTaxi" => "11:00",
            "takeOff" => "11:08"],
        "arrival" => [
            "landingPlace" => "OUL",
            "landingTime" => "12:00",
            "engineOff" => "12:05"
        ],
        "planeId" => "1",
        "aircraftRegistration" => "MVMS-2321",
        "aircraftModel" => "Boeing",
        "aircraftVariant" => "Airbus 320",
        "singlePilotTimeSingleEngine" => "5",
        "singlePilotTimeMultiEngine" => "0",
        "flightTime" => "52",
        "pilotName" => "Self",
        "landingsDay" => "1",
        "landingsNight" => "2",
        "opConditionTime" => [
            "opConditionTimeNight" => "1",
            "opConditionTimeIFR" => "2",
        ],
            "pilotFuncTimePIC" => "2",
        "pilotFunctTimeCoPilot" => "1",
        "pilotFuncTimeDual" => "1",
        "pilotFunctTimeInstructor" => "20",
        "remarksAndEndorsments" => "you are a very good pilot, mate",
        "nightRatingTraining" => "good",
        "proficiencyCheck" => "check"

    );

    public $organizationFlight = array("id" => "2",
        "date" => "08/11/2017",
        "pilotId" => "1",
        "flightType" => "Type of flight",
        "startType" => "Type of start",
        "departure" => [
            "departurePlace" => "HEL",
            "startTaxi" => "11:00",
            "takeOff" => "11:08"],
        "arrival" => [
            "landingPlace" => "OUL",
            "landingTime" => "12:00",
            "engineOff" => "12:05"
        ],
        "planeId" => "1",
        "aircraftRegistration" => "MVMS-2321",
        "aircraftModel" => "Boeing",
        "aircraftVariant" => "Airbus 320",
        "singlePilotTimeSingleEngine" => "5",
        "singlePilotTimeMultiEngine" => "0",
        "flightTime" => "52",
        "pilotName" => "Self",
        "landingsDay" => "1",
        "landingsNight" => "2",
        "opConditionTime" => [
            "opConditionTimeNight" => "1",
            "opConditionTimeIFR" => "2",
        ],
        "pilotFuncTimePIC" => "2",
        "pilotFunctTimeCoPilot" => "1",
        "pilotFuncTimeDual" => "1",
        "pilotFunctTimeInstructor" => "20",
        "remarksAndEndorsments" => "you are a very good pilot, mate",
        "nightRatingTraining" => "good",
        "proficiencyCheck" => "check"

    );


    /**
     * @Rest\Get("/flights")
     */
    public function getAction()
    {


        $restresult = $this->flight;
        if ($restresult === null) {
            return new View("There are no flights. Please insert one.", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * @Rest\Get("/flights/organizations/1")
     */
    public function getFlights()
    {
        $singleresult = $this->flight;
        if ($singleresult === null) {
            return new View("Flight not found", Response::HTTP_NOT_FOUND);
        }
        return $singleresult;
    }

    /**
     * @Rest\Post("/flights/")
     */
    public function postAction(Request $request)
    {
        $date = $request->get('date');
        $flightType = $request->get('flightType');
        $startType = $request->get('startType');
        $departurePlace = $request->get('departurePlace');
        $startTaxi = $request->get('startTaxi');
        $takeOff = $request->get('takeOff');
        $landingPlace = $request->get('landingPlace');
        $landingTime = $request->get('landingTime');
        $engineOff = $request->get('engineOff');
        $aircraftRegistration = $request->get('aircraftRegistration');
        $aircraftModel = $request->get('aircraftModel');
        $aircraftVariant = $request->get('aircraftVariant');
        $singlePilotTimeSingleEngine = $request->get('singlePilotTimeSingleEngine');
        $singlePilotTimeMultiEngine = $request->get('singlePilotTimeMultiEngine');
        $pilotName = $request->get('pilotName');
        $landingsDay = $request->get('landingsDay');
        $landingsNight = $request->get('landingsNight');
        $opConditionTimeNight = $request->get('opConditionTimeNight');
        $opConditionTimeIFR = $request->get('opConditionTimeIFR');
        $pilotFuncTimePIC = $request->get('pilotFunctTimePic');
        $pilotFuncTimeCoPilot= $request->get('pilotFuncTimeCoPilot');
        $pilotFuncTimeDual= $request->get('pilotFuncTimeDual');
        $pilotFunctTimeInstructor = $request->get('pilotFuncTimeInstructor');
        $remarksAndEndorsments = $request->get('remarksAndEndorsments');
        $nightRatingTraining = $request->get('nightRatingTraining');
        $proficiencyCheck = $request->get('proficiencyCheck');

        //Check if $date is a date

        if (DateTime::createFromFormat('Y-m-d', $date) == false)
        {
            return new View("The flight is on an invalid date!", Response::HTTP_NOT_ACCEPTABLE);
        }

        //Check if $date is a valid date

        $errors = DateTime::getLastErrors();
        if (!empty($errors['warning_count'])) {
            return new View("The flight is on an invalid date!", Response::HTTP_NOT_ACCEPTABLE);
        }


        //Check if valid time

        function testTime($check) {
            $time = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $check);
            return $time;
        }

            if(testTime($startTaxi) == 0) {
                return new View("Start taxi time is invalid", Response::HTTP_NOT_ACCEPTABLE);
            }

            if(testTime($takeOff) == 0) {
                return new View("Take off time is invalid", Response::HTTP_NOT_ACCEPTABLE);
            }

            if(testTime($landingTime) == 0) {
                return new View("Landing time is invalid", Response::HTTP_NOT_ACCEPTABLE);
            }

            if(testTime($engineOff) == 0) {
                return new View("Engine off time is invalid", Response::HTTP_NOT_ACCEPTABLE);
            }

        //Check if it's not null

        if ($flightType === null || $departurePlace === null || $landingPlace === null || $aircraftRegistration === null
            || $aircraftModel === null || $aircraftVariant === null || $pilotName === null ) {
            return new View('Please fill in all the required data.', Response::HTTP_NOT_ACCEPTABLE);
        }


        return new View("Flight on ".$date." has been added. Flight is from ".$departurePlace." to ".$landingPlace);

    }
}