<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Flight
 *
 * @ORM\Table(name="flight")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FlightRepository")
 */
class Flight
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="departure", type="string", length=16)
     */
    private $departure;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startTaxi", type="datetime")
     */
    private $startTaxi;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="takeOff", type="datetime")
     */
    private $takeOff;

    /**
     * @var string
     *
     * @ORM\Column(name="arrival", type="string", length=16)
     */
    private $arrival;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="landing", type="datetime")
     */
    private $landing;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="engineOff", type="datetime")
     */
    private $engineOff;

    /**
     * @var string
     *
     * @ORM\Column(name="aircraft", type="string", length=255)
     */
    private $aircraft;

    /**
     * @var string
     *
     * @ORM\Column(name="aicraftRegistration", type="string", length=255)
     */
    private $aicraftRegistration;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Flight
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set departure
     *
     * @param string $departure
     *
     * @return Flight
     */
    public function setDeparture($departure)
    {
        $this->departure = $departure;

        return $this;
    }

    /**
     * Get departure
     *
     * @return string
     */
    public function getDeparture()
    {
        return $this->departure;
    }

    /**
     * Set startTaxi
     *
     * @param \DateTime $startTaxi
     *
     * @return Flight
     */
    public function setStartTaxi($startTaxi)
    {
        $this->startTaxi = $startTaxi;

        return $this;
    }

    /**
     * Get startTaxi
     *
     * @return \DateTime
     */
    public function getStartTaxi()
    {
        return $this->startTaxi;
    }

    /**
     * Set takeOff
     *
     * @param \DateTime $takeOff
     *
     * @return Flight
     */
    public function setTakeOff($takeOff)
    {
        $this->takeOff = $takeOff;

        return $this;
    }

    /**
     * Get takeOff
     *
     * @return \DateTime
     */
    public function getTakeOff()
    {
        return $this->takeOff;
    }

    /**
     * Set arrival
     *
     * @param string $arrival
     *
     * @return Flight
     */
    public function setArrival($arrival)
    {
        $this->arrival = $arrival;

        return $this;
    }

    /**
     * Get arrival
     *
     * @return string
     */
    public function getArrival()
    {
        return $this->arrival;
    }

    /**
     * Set landing
     *
     * @param \DateTime $landing
     *
     * @return Flight
     */
    public function setLanding($landing)
    {
        $this->landing = $landing;

        return $this;
    }

    /**
     * Get landing
     *
     * @return \DateTime
     */
    public function getLanding()
    {
        return $this->landing;
    }

    /**
     * Set engineOff
     *
     * @param \DateTime $engineOff
     *
     * @return Flight
     */
    public function setEngineOff($engineOff)
    {
        $this->engineOff = $engineOff;

        return $this;
    }

    /**
     * Get engineOff
     *
     * @return \DateTime
     */
    public function getEngineOff()
    {
        return $this->engineOff;
    }

    /**
     * Set aircraft
     *
     * @param string $aircraft
     *
     * @return Flight
     */
    public function setAircraft($aircraft)
    {
        $this->aircraft = $aircraft;

        return $this;
    }

    /**
     * Get aircraft
     *
     * @return string
     */
    public function getAircraft()
    {
        return $this->aircraft;
    }

    /**
     * Set aicraftRegistration
     *
     * @param string $aicraftRegistration
     *
     * @return Flight
     */
    public function setAicraftRegistration($aicraftRegistration)
    {
        $this->aicraftRegistration = $aicraftRegistration;

        return $this;
    }

    /**
     * Get aicraftRegistration
     *
     * @return string
     */
    public function getAicraftRegistration()
    {
        return $this->aicraftRegistration;
    }
}

